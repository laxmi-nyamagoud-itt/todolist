import React, { useState } from 'react';
import TodoForm from './components/TodoForm';


const App: React.FC = ({ }) => {
  return (
    <>
      <div className='container'>
        <TodoForm />
      </div>
    </>
  );
};

export default App;
