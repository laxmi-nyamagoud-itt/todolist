import React, { useState } from 'react';
import '../TodoList.css'

type Todo = {
    text: string;
    id: number;
}

interface TodoListProps {
    todos: Array<Todo>;
    setTodos: any;
    setNewTodo: any;
    setEditId: any;
};

const TodoList: React.FC<TodoListProps> = ({ todos, setTodos, setNewTodo, setEditId }) => {
    //Delete todo from todos array
    const deleteTodo = (id: number) => {
        setTodos([...todos].filter((todo) => todo.id !== id))
    }

    const handleEditing = (id: number) => {
        const editTodo = todos.find((t) => t.id === id);
        if (editTodo) {
            setNewTodo(editTodo.text)
        }
        setEditId(id);
    }

    return (
        <>
            {todos.map(todo => {
                return (
                    <ul className='todoList'>
                        <li className='text' >
                            {todo.text}
                        </li>
                        <div>
                            <button className='edit' onClick={() => handleEditing(todo.id)}>Edit</button>
                            <button className='deleteButton' onClick={() => deleteTodo(todo.id)} >Delete</button>
                        </div>

                    </ul>
                )
            })}
        </>
    );
};

export default TodoList;
