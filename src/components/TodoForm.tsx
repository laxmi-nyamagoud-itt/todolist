import React, { useState } from 'react';
import TodoList from './TodoList';
import '../TodoList.css'
import image from '../image.jpg';

type Todo = {
    text: string;
    id: number;
}

const TodoForm: React.FC = ({ }) => {
    const [todos, setTodos] = useState([] as any);
    const [newTodo, setNewTodo] = useState("");
    const [editId, setEditId] = useState(0);

    //add newTodo to todos array
    const addTodo = (newTodo: string) => {
        newTodo.trim() !== "" &&
            setTodos([...todos, { text: newTodo, id: Math.floor(Math.random() * 1000) }])
    }

    const handleChange = (e: any) => {
        setNewTodo(e.target.value)
    };

    const handleSubmit = (e: any) => {
        e.preventDefault();
        //Edit todo
        if (editId) {
            const editTodo = todos.find((todo: Todo) => todo.id === editId);
            const updatedTodos = todos.map((todo: Todo) =>
                todo.id === editTodo.id
                    ? (todo = { id: todo.id, text: newTodo })
                    : { id: todo.id, text: todo.text }
            );
            console.log(updatedTodos)
            setTodos(updatedTodos);
            setEditId(0);
            setNewTodo("");
            return;
        }

        addTodo(newTodo);
        setNewTodo("");
    };

    return (
        <>
            <h1>Todo-List</h1>
            <img src={image} alt="todo" className='image' />
            <form className='form'>
                <input type="text" value={newTodo} onChange={handleChange} className='inputField' />
                <button type="submit" onClick={handleSubmit} className='addTodo'>{editId ? "Edit" : "Add Todo"}</button>
            </form>
            <TodoList todos={todos} setTodos={setTodos} setNewTodo={setNewTodo} setEditId={setEditId} />
        </>
    );
};

export default TodoForm;
